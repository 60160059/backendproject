"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var hourSchema = new mongoose.Schema({
  idPerson: String,
  name: String,
  branch: String,
  ready: Number,
  academic: Number
});
module.exports = mongoose.model('Hour', hourSchema);