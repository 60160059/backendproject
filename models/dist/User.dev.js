"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var userSchema = new mongoose.Schema({
  name: String,
  type: String,
  date: Date,
  time: String,
  room: String,
  hours: Number
});
module.exports = mongoose.model('User', userSchema);