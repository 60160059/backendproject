const mongoose = require('mongoose')
const Schema = mongoose.Schema
const hourSchema = new mongoose.Schema({
  idPerson: String,
  name: String,
  branch: String,
  ready: Number,
  academic: Number
})

module.exports = mongoose.model('Hour', hourSchema)