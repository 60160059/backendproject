const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new mongoose.Schema({
  name: String,
  type: String,
  date: Date,
  time: String,
  room: String,
  hours: Number
})

module.exports = mongoose.model('User', userSchema)