const express = require('express');
const router = express.Router();
const hoursController = require('../controller/HourController')
const User = require('../models/Hour')

/* GET users listing. */
router.get('/', hoursController.getUsers)

router.get('/:id', hoursController.getUser)

router.post('/', hoursController.addUser)

router.put('/', hoursController.updateUser)

router.delete('/:id', hoursController.deleteUser)

module.exports = router;
