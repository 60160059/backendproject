"use strict";

var express = require('express');

var router = express.Router();

var hoursController = require('../controller/HourController');

var User = require('../models/Hour');
/* GET users listing. */


router.get('/', hoursController.getUsers);
router.get('/:id', hoursController.getUser);
router.post('/', hoursController.addUser);
router.put('/', hoursController.updateUser);
router["delete"]('/:id', hoursController.deleteUser);
module.exports = router;