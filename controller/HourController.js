const User = require('../models/Hour')
const hourController = {
  userList: [
    {
      id: 1,
      idPerson: '59160020',
      name: 'นายประจักร ขมักขมิ่น',
      branch: 'วิทยาการคอมพิวเตอร์',
      ready: '21',
      academic: '8'
    },
    {
      id: 2,
      idPerson: '59160123',
      name: 'นายสมชาย แมนมาก',
      branch: 'วิทยาการคอมพิวเตอร์',
      date: '16 กุมภาพันธ์  พ.ศ.2562',
      ready: '30',
      academic: '15'
    }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // User.create(payload).then(function (user) {
    //   res.json(user)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    // res.json(usersController.addUser(payload))
    console.log(payload)
    const user = new User(payload)
    try {
      const newuser = await user.save()
      res.json(newuser)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    //Pattern 1
    // User.find({}).exec(function (err, users) {
    //   if (err) {
    //     res.status(500).send()
    //   }
    //   res.json(users)
    // })
  
    //Pattern 2
    // User.find({}).then(function (users) {
    //   res.json(users)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
  
    // Async Await
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = hourController
