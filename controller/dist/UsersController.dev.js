"use strict";

var User = require('../models/User');

var userController = {
  userList: [{
    id: 1,
    name: 'UX/UI Design',
    type: 'วิชาการ',
    date: '16 กุมภาพันธ์  พ.ศ.2562',
    time: '13.00-16.00',
    room: 'ห้องประชุม IF-11M280',
    hours: '3'
  }, {
    id: 2,
    name: 'G-able',
    type: 'เตรียมความพร้อม',
    date: '5 กันยายน พ.ศ.2562',
    time: '13.00-16.00',
    room: 'ห้องประชุม IF-4M210',
    hours: '3'
  }],
  lastId: 3,
  addUser: function addUser(req, res, next) {
    var payload, user, newuser;
    return regeneratorRuntime.async(function addUser$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            payload = req.body; // User.create(payload).then(function (user) {
            //   res.json(user)
            // }).catch(function (err) {
            //   res.status(500).send(err)
            // })
            // res.json(usersController.addUser(payload))

            console.log(payload);
            user = new User(payload);
            _context.prev = 3;
            _context.next = 6;
            return regeneratorRuntime.awrap(user.save());

          case 6:
            newuser = _context.sent;
            res.json(newuser);
            _context.next = 13;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](3);
            res.status(500).send(_context.t0);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[3, 10]]);
  },
  updateUser: function updateUser(req, res, next) {
    var payload, user;
    return regeneratorRuntime.async(function updateUser$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            payload = req.body; // res.json(usersController.updateUser(payload))

            _context2.prev = 1;
            _context2.next = 4;
            return regeneratorRuntime.awrap(User.updateOne({
              _id: payload._id
            }, payload));

          case 4:
            user = _context2.sent;
            res.json(user);
            _context2.next = 11;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            res.status(500).send(_context2.t0);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  deleteUser: function deleteUser(req, res, next) {
    var id, user;
    return regeneratorRuntime.async(function deleteUser$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id; // res.json(usersController.deleteUser(id))

            _context3.prev = 1;
            _context3.next = 4;
            return regeneratorRuntime.awrap(User.deleteOne({
              _id: id
            }));

          case 4:
            user = _context3.sent;
            res.json(user);
            _context3.next = 11;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](1);
            res.status(500).send(_context3.t0);

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getUsers: function getUsers(req, res, next) {
    var users;
    return regeneratorRuntime.async(function getUsers$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return regeneratorRuntime.awrap(User.find({}));

          case 3:
            users = _context4.sent;
            res.json(users);
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](0);
            res.status(500).send(_context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getUser: function getUser(req, res, next) {
    var id, user;
    return regeneratorRuntime.async(function getUser$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return regeneratorRuntime.awrap(User.findById(id));

          case 4:
            user = _context5.sent;
            res.json(user);
            _context5.next = 11;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            res.status(500).send(_context5.t0);

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 8]]);
  }
};
module.exports = userController;